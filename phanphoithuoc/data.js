var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa khoa Tỉnh Sóc Trăng",
   "address": "17 Pasteur, KhóM 5, PhườNg 8, thành phố Sóc Trăng, tỉnh Sóc Trăng",
   "Longtitude": 9.594836,
   "Latitude": 105.988671
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Quân Dân Y tỉnh Sóc Trăng",
   "address": "Số: 377 Nguyễn Văn Linh, Phường 2, thành phố Sóc Trăng, tỉnh Sóc Trăng",
   "Longtitude": 9.591405,
   "Latitude": 105.965438
 },
 {
   "STT": 3,
   "Name": "Bệnh viện 30 Tháng 4 Sóc Trăng",
   "address": "468, Đường 30/4, Khóm 1, Phường 3, thành phố Sóc Trăng, tỉnh Sóc Trăng",
   "Longtitude": 9.581152,
   "Latitude": 105.98702
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Đa khoa huyện Cù Lao Dung",
   "address": "Thị Trấn Cù Lao Dung, huyền Cù Lao Dung, tỉnh Sóc Trăng",
   "Longtitude": 9.673796,
   "Latitude": 106.154513
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Đa khoa huyện Kế Sách",
   "address": "Ấp An Phú- Thị Trấn Kế Sách, huyện Kế Sách, tỉnh Sóc Trăng",
   "Longtitude": 9.762063,
   "Latitude": 105.983089
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa huyện Long Phú",
   "address": "Ấp 2, Thị Trấn Long Phú, huyện Long Phú, tỉnh Sóc Trăng",
   "Longtitude": 9.6129102,
   "Latitude": 106.1306955
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Đa khoa huyện Mỹ Tú",
   "address": "Số 01 Trần Phú, Mỹ Thuận, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, tỉnh Sóc Trăng",
   "Longtitude": 9.6128819,
   "Latitude": 105.9949854
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa khoa huyện Mỹ Xuyên",
   "address": "1 Lý Thường Kiệt, Thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, tỉnh Sóc Trăng",
   "Longtitude": 9.5598262,
   "Latitude": 105.9885701
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Đa khoa huyện Ngã Năm",
   "address": "Ấp 1 Thị Trấn Ngã Năm, huyện Ngã Năm, tỉnh Sóc Trăng",
   "Longtitude": 9.560142,
   "Latitude": 105.601223
 },
 {
   "STT": 10,
   "Name": "Bệnh viện Đa khoa huyện Thạnh Trị",
   "address": "Đường Lý Tự Trọng, Ấp I, Thị trấn Phú Lộc, huyện Thạnh Trị, tỉnh Sóc Trăng",
   "Longtitude": 9.438429,
   "Latitude": 105.737106
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Đa khoa thị xã Vĩnh Châu",
   "address": "Nguyễn Huệ Khóm 1 Phường 1, thị xã Vĩnh Châu, tỉnh Sóc Trăng",
   "Longtitude": 9.323194,
   "Latitude": 105.975313
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Đa khoa thành phố Sóc Trăng",
   "address": "10 Hai Bà Trưng, thành phố Sóc Trăng, tỉnh Sóc Trăng",
   "Longtitude": 9.594829,
   "Latitude": 105.98867
 },
 {
   "STT": 13,
   "Name": "Bệnh viện Mắt Sóc Trăng",
   "address": "689, Lê Hồng Phong, Phường 3,Thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.57845,
   "Latitude": 105.977537
 },
 {
   "STT": 14,
   "Name": "Trung tâm Y tế dự phòng Sóc Trăng",
   "address": "506 Lê Hồng Phong, Phường 3, Thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.579511,
   "Latitude": 105.976432
 },
 {
   "STT": 15,
   "Name": "Trung tâm Y tế huyện Châu Thành ",
   "address": "QL1A, Thị Trấn Châu Thành, Châu Thành, Sóc Trăng",
   "Longtitude": 9.706164,
   "Latitude": 105.897587
 },
 {
   "STT": 16,
   "Name": "Trung tâm Y tế huyện Trần Đề",
   "address": "Hai Bà Trưng, Lịch Hội Thượng, Trần Đề, Sóc Trăng",
   "Longtitude": 9.488186,
   "Latitude": 106.146606
 },
 {
   "STT": 17,
   "Name": "Trung tâm Y tế huyện Kế Sách",
   "address": "Thị Trấn Kế Sách, Huyện Kế Sách, Tỉnh Sóc Trăng",
   "Longtitude": 9.762402,
   "Latitude": 105.984218
 },
 {
   "STT": 18,
   "Name": "Trung tâm Y tế huyện Mỹ Tú",
   "address": "Thị Trấn Huỳnh Hữu Nghĩa, Huyện Mỹ Tú, Tỉnh Sóc Trăng",
   "Longtitude": 9.637124,
   "Latitude": 105.799677
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Mỹ Xuyên",
   "address": "Triệu Nương, Thị trấn  Mỹ Xuyên, Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.559124,
   "Latitude": 105.988544
 },
 {
   "STT": 20,
   "Name": "Trung tâm Y tế huyện Long Phú",
   "address": "Ấp 2, Thị Trấn Long Phú, Huyện Long Phú, Sóc Trăng\n\n",
   "Longtitude": 9.609266,
   "Latitude": 106.135227
 },
 {
   "STT": 21,
   "Name": "Trung tâm Y tế thị xã Vĩnh Châu",
   "address": "đường Nguyễn Huệ, khóm 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.323477,
   "Latitude": 105.974653
 },
 {
   "STT": 22,
   "Name": "Trung tâm Y tế huyện Cù Lao Dung",
   "address": "đường 30/4 ấp Phước Hòa B,, An Thạnh Đông, Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.658599,
   "Latitude": 106.155929
 },
 {
   "STT": 23,
   "Name": "Trung tâm Y tế huyện Ngã Năm",
   "address": "Đường Mai Thanh Thế, Thị trấn  Ngã Năm, tx  Ngã Năm, Sóc Trăng",
   "Longtitude": 9.562978,
   "Latitude": 105.595977
 }
];