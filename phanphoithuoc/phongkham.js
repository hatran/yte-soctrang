var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng Khám Răng Hàm Mặt",
   "address": "Số 55, đường Trần Hưng Đạo, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5991821,
   "Latitude": 105.972501
 },
 {
   "STT": 2,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 2333, đường Hùng Vương, Khóm 6, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6127501,
   "Latitude": 105.9685738
 },
 {
   "STT": 3,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 134, Quốc Lộ 1A, Khóm 1, Phường 7, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.62628,
   "Latitude": 105.9583852
 },
 {
   "STT": 4,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 124, đường Lê Hồng Phong, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.593005,
   "Latitude": 105.974519
 },
 {
   "STT": 5,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 13, đường Lê Hồng Phong, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.593005,
   "Latitude": 105.974519
 },
 {
   "STT": 6,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 95, đường Lê Hồng Phong, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5860528,
   "Latitude": 105.9758484
 },
 {
   "STT": 7,
   "Name": "Phòng Chẩn Trị Đông Y Vĩnh Thọ Đường",
   "address": "Số 80, Ấp An Ninh I, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": 8,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 121, đường Ung Công Uẩn, Ấp An Thành, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.76845159999999,
   "Latitude": 105.9816794
 },
 {
   "STT": 9,
   "Name": "Phòng Răng Hải Triều",
   "address": "Số 20, đường Bạch Đằng, Ấp An Ninh I, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7697012,
   "Latitude": 105.9861087
 },
 {
   "STT": 10,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Ninh Thới, Xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": 11,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": " Số 268/KVIII, Ấp An Ninh 2, Xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": 12,
   "Name": "Nha Công Nguyễn Văn Sanh",
   "address": "Số 134, Ấp Dương Kiển, Xã Hòa Tú 2, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4496457,
   "Latitude": 105.8936224
 },
 {
   "STT": 13,
   "Name": "Phòng Khám Bệnh Nội Hô Hấp Bác Sỹ Danh Vơi",
   "address": "Số 18, Quốc lộ IA, Ấp Tâm Lộc, Xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5428648,
   "Latitude": 105.9141793
 },
 {
   "STT": 14,
   "Name": "Phòng Răng Hữu Nghị Ii",
   "address": "Số 53, đường Hoàng Diệu, Ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5601205,
   "Latitude": 105.9886104
 },
 {
   "STT": 15,
   "Name": "Phòng Răng Hưng Phát",
   "address": "Số 38, đường Hàm Nghi, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6036402,
   "Latitude": 105.9747151
 },
 {
   "STT": 16,
   "Name": "Phòng Răng Tân Mỹ",
   "address": "Số 186, ấp Phước Lợi, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67307999999999,
   "Latitude": 105.9523622
 },
 {
   "STT": 17,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 116/1, Ấp Đông Hải, Xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.77867779999999,
   "Latitude": 105.8877494
 },
 {
   "STT": 18,
   "Name": "Tuệ Tĩnh Đường Ngọc Tâm",
   "address": "Số 02 Khu I, Ấp Phước An, Xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": 19,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi",
   "address": "Số 232, ấp Phước Lợi, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": 20,
   "Name": "Nha Khoa Ngọc Hạnh",
   "address": "Số 59 Nguyễn Trung Trực, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59679929999999,
   "Latitude": 105.9686483
 },
 {
   "STT": 21,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Dân Tộc",
   "address": "Ấp Xây Đá B, Xã Hồ Đắc Kiện, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.7150088,
   "Latitude": 105.8642593
 },
 {
   "STT": 22,
   "Name": "Phòng Chẩn Trị Đông Y Quảng Vinh Xương",
   "address": "Số 278/15A, đường Trương Công Định, Khóm 5, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60366069999999,
   "Latitude": 105.9664128
 },
 {
   "STT": 23,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 155, đường Xô Viết Nghệ Tĩnh, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6071796,
   "Latitude": 105.9726434
 },
 {
   "STT": 24,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 133 - 135, đường Phú Lợi, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.595995,
   "Latitude": 105.9690456
 },
 {
   "STT": 25,
   "Name": "Phòng Răng Nghị Thành",
   "address": "Số 78 đường 30/4, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": 26,
   "Name": "Phòng Răng Thành Nhơn",
   "address": "Số 82 Trương Công Định, Khóm 6, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59451889999999,
   "Latitude": 105.9611748
 },
 {
   "STT": 27,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 73, đường Tôn Đức Thắng, Khóm 5, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6093361,
   "Latitude": 105.977294
 },
 {
   "STT": 28,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 9, đường Thủ Khoa Huân, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6009845,
   "Latitude": 105.974874
 },
 {
   "STT": 29,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 132/1, đường Điện Biên Phủ, Khóm 3, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6048068,
   "Latitude": 105.9773504
 },
 {
   "STT": 30,
   "Name": "Phòng Răng Tuấn Dân",
   "address": "Số 14, đường Phan Đình Phùng, Ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5594438,
   "Latitude": 105.9853184
 },
 {
   "STT": 31,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 44, đường Lê Văn Tám, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5836969,
   "Latitude": 105.9630114
 },
 {
   "STT": 32,
   "Name": "Phòng Răng Hồng Dân",
   "address": "Số 745, Quốc Lộ IA, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.595995,
   "Latitude": 105.9690456
 },
 {
   "STT": 33,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 189, đường Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6009768,
   "Latitude": 105.9698046
 },
 {
   "STT": 34,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 281, đường Trương Công Định, Khóm 4, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6036687,
   "Latitude": 105.9658658
 },
 {
   "STT": 35,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Ấp II, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.60925389999999,
   "Latitude": 106.135192
 },
 {
   "STT": 36,
   "Name": "Phòng Khám Bác Sỹ Đức",
   "address": "Số 205, Ấp Ngãi Hội 2, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": 37,
   "Name": "Phòng Răng Lê Văn Đôi",
   "address": "Số 659, Ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": 38,
   "Name": "Nha Khoa Phan Thượng",
   "address": "Số 273, Quốc Lộ 1A, Khóm 1, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.57469869999999,
   "Latitude": 105.9575561
 },
 {
   "STT": 39,
   "Name": "Phòng Răng Ngọc Huệ",
   "address": "Số 125, đường Lê Lợi, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58877519999999,
   "Latitude": 105.962917
 },
 {
   "STT": 40,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 13, đường Nguyễn Văn Linh, Khóm 6, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59813239999999,
   "Latitude": 105.9643186
 },
 {
   "STT": 42,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 46, đường Trần Hưng Đạo, Khóm 2, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58409829999999,
   "Latitude": 105.9631303
 },
 {
   "STT": 43,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 301, đường Lê Hồng Phong, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5860528,
   "Latitude": 105.9758484
 },
 {
   "STT": 44,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 83, đường Lê Hồng Phong, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5860528,
   "Latitude": 105.9758484
 },
 {
   "STT": 45,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 19, đường Đinh Tiên Hoàng, Khóm 2, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6047806,
   "Latitude": 105.9738211
 },
 {
   "STT": 46,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 72, đường Lý Thường Kiệt, Khóm 4, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5895761,
   "Latitude": 105.9637016
 },
 {
   "STT": 47,
   "Name": "Phòng Răng Trần Văn Hưởng",
   "address": "Ấp Xóm Lớn, xã Mỹ Hương, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": 48,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 242, ấp Nội Ô, thị trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": 49,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 56, đường Mai Thanh Thế, Khóm 4, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60191369999999,
   "Latitude": 105.9779895
 },
 {
   "STT": 50,
   "Name": "Phòng Chẩn Trị Đông Y Vạn Đức An",
   "address": "Số Ơ-LK33-01, đường số 8, Khóm 4, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5899393,
   "Latitude": 105.9640035
 },
 {
   "STT": 52,
   "Name": "Phòng Xét Nghiệm Y Khoa Thắng Vân",
   "address": "Số 50, đường Nguyễn Thị Minh Khai, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5995298,
   "Latitude": 105.9749447
 },
 {
   "STT": 55,
   "Name": "Phòng Răng Ngọc Hằng",
   "address": "Đường Nguyễn Huệ, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5601298,
   "Latitude": 105.6012096
 },
 {
   "STT": 56,
   "Name": "Phòng Răng Tâm Như",
   "address": "Số 313, Ấp I, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52151709999999,
   "Latitude": 105.6177942
 },
 {
   "STT": 57,
   "Name": "Phòng Chẩn Trị Đông Y \"Chấn Nguyên Đường\"",
   "address": "Số 16 A, ấp Mỹ Thành, xã Mỹ Quới, huyện Ngã Năm., Sóc Trăng",
   "Longtitude": 9.453873,
   "Latitude": 105.5650239
 },
 {
   "STT": 58,
   "Name": "Nha Khoa Hữu Nghị 3",
   "address": "Số 36, Quốc Lộ 1 A, ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4306994,
   "Latitude": 105.7475012
 },
 {
   "STT": 59,
   "Name": "Phòng Khám Chuyên Khoa \"Các Phượng\"",
   "address": "Số 227, đường Nguyễn Trung Trực, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60021759999999,
   "Latitude": 105.9702906
 },
 {
   "STT": 60,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 170, ấp Xóm Lớn, xã Mỹ Hương, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": 61,
   "Name": "Phòng Chẩn Trị Đông Y \"Vạn Phước Đường\"",
   "address": "Ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": 62,
   "Name": "Phòng Chẩn Trị \"Vạn Xuân Đường\"",
   "address": "Ấp An Ninh II, xã An Lạc Thôn, thị trấn Kế Sách, huyện kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": 63,
   "Name": "Phòng Chẩn Trị \"Hưng Thịnh\"",
   "address": "Số 72, đường Đoàn Thế Trung, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6653018,
   "Latitude": 106.1510785
 },
 {
   "STT": 64,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Từ Thiện\"",
   "address": "Ấp Mỹ Hội, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": 65,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp An Nghiệp, Xã An Thạnh 3, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.5667077,
   "Latitude": 106.2639825
 },
 {
   "STT": 66,
   "Name": "Nha Khoa \"Tuấn Mậu\"",
   "address": "Số 275, Nguyễn Huệ, Khóm 1, Phường 1, thành phố Sóc Trăng , Sóc Trăng",
   "Longtitude": 9.57469869999999,
   "Latitude": 105.9575561
 },
 {
   "STT": 67,
   "Name": "Phòng Khám \"Bác Sỹ Chung Tấn Định\"",
   "address": "Số 192-194, đường Trần BìnhTrọng, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6011002,
   "Latitude": 105.9700207
 },
 {
   "STT": 68,
   "Name": "Phòng Chẩn Trị \"Đức Thảo Đường\"",
   "address": "Số 69, Đoàn Thế Trung, ấp Chợ, thị trấn Cù lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6683191,
   "Latitude": 106.1536252
 },
 // {
 //   "STT": 69,
 //   "Name": "Phòng Răng Giả \"Vĩnh Thạnh\"",
 //   "address": "Số 588, Ấp Châu Thành, thị trấn Lịch Hội Thượng, huyện Trần Đề, Sóc Trăng",
 //   "Longtitude": 22.5516542,
 //   "Latitude": 114.2696974
 // },
 {
   "STT": 70,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Phan Văn Lệnh",
   "address": "Số 49, đường Mai Thanh Thế, Khóm 2, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60191369999999,
   "Latitude": 105.9779895
 },
 {
   "STT": 71,
   "Name": "Phòng Khám Nha Khoa Sóc Trăng",
   "address": "Số 67, đường Lê Lợi, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085238,
   "Latitude": 105.9738761
 },
 {
   "STT": 72,
   "Name": "Phòng Khám Răng Hàm Mặt",
   "address": "Số 215, Nguyễn Trung Trực, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6008665,
   "Latitude": 105.9704811
 },
 {
   "STT": 80,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 593, đường 30 tháng 4, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": 81,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 367, đường Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5796503,
   "Latitude": 105.9771829
 },
 {
   "STT": 83,
   "Name": "Phòng Khám Nội Tổng Hợp \"Út Liễu\"",
   "address": "Ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": 84,
   "Name": "Nha Khoa \"Hải\"",
   "address": "Số 94, đường Đồng Khởi, Khóm 1, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6030753,
   "Latitude": 105.9759916
 },
 {
   "STT": 85,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 4, đường Xô Viết Nghệ Tĩnh, Khóm 3, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6048068,
   "Latitude": 105.9773504
 },
 {
   "STT": 89,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 206, đường Trương Công Định, Khóm 5, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60366069999999,
   "Latitude": 105.9664128
 },
 {
   "STT": 90,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu",
   "address": "Số 105, đường Văn Ngọc Chính, Khóm 9, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5838503,
   "Latitude": 105.9765083
 },
 {
   "STT": 91,
   "Name": "Phòng Khám Tâm Thần Kinh",
   "address": "Số 369/21, đường Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5860528,
   "Latitude": 105.9758484
 },
 {
   "STT": 92,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản",
   "address": "Số 367, đường Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5796503,
   "Latitude": 105.9771829
 },
 {
   "STT": 93,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Văn Hổ\"",
   "address": "Ấp Phương Thạnh 2, xã Hưng Phú, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6568607,
   "Latitude": 105.7233814
 },
 {
   "STT": 96,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu",
   "address": "Số 02A, đường Pasteur, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6165731,
   "Latitude": 106.0107648
 },
 {
   "STT": 97,
   "Name": "Phòng Khám \"Nha Khoa Minh Tân Sóc Trăng\"",
   "address": "Số 91 Lê Lợi, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60896889999999,
   "Latitude": 105.9730733
 },
 {
   "STT": 98,
   "Name": "Phòng Chẩn Trị Đông Y \"Hòa Hưng\"",
   "address": "Số 5, đường Hoàng Diệu, Khóm 1, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6031143,
   "Latitude": 105.9749556
 },
 {
   "STT": 100,
   "Name": "Phòng Chẩn Trị \"Lâm Phát Đường\"",
   "address": "Ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": 101,
   "Name": "Phòng Khám \"Bác Sỹ Thái\"",
   "address": "Số 21 Nguyễn Chí Thanh, Khóm 5, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60949299999999,
   "Latitude": 105.9760075
 },
 {
   "STT": 105,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Lương Thanh Dũng\"",
   "address": "Số 113, ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, , Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": 106,
   "Name": "Phòng Khám \"Bác Sỹ Nguyễn Công Nghiệp\"",
   "address": "Số 65, Khóm Xẻo Me, Phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": 107,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 204, đường 3/2, Ấp 1, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56488959999999,
   "Latitude": 105.5972847
 },
 {
   "STT": 108,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 125, Ấp 1, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52151709999999,
   "Latitude": 105.6177942
 },
 {
   "STT": 109,
   "Name": "Phòng Khám \"Bác Sỹ Nguyễn Văn Vạn",
   "address": "Số 459, Nguyễn Trung Trực, Ấp 1, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52151709999999,
   "Latitude": 105.6177942
 },
 {
   "STT": 110,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Công Thiện\"",
   "address": "Số 17, đường Hai Bà Trưng, Khóm 2, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6044191,
   "Latitude": 105.9747132
 },
 {
   "STT": 111,
   "Name": "Dịch Vụ Làm Răng Giả \"Mạnh Hùng\"",
   "address": "Số nhà A86/3, Ấp 3, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.6221576,
   "Latitude": 106.1169299
 },
 {
   "STT": 112,
   "Name": "Phòng Răng \"Pho II\"",
   "address": "Ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73177029999999,
   "Latitude": 106.0696859
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Thiện",
   "address": "Số 615, ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phú Giao, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp 3, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Trần Bích Thương",
   "address": "Số 459, Nguyễn Trung Trực, Ấp 1, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52151709999999,
   "Latitude": 105.6177942
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 27, Quốc lộ 1A , Ấp 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.3984115,
   "Latitude": 105.7144082
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Xa Mau 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.43187249999999,
   "Latitude": 105.7505734
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Trương Hiền, xã Thạnh Trị, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4639086,
   "Latitude": 105.6999122
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Quý",
   "address": "Ấp Xa Mau 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 35, ấp Chợ cũ, thị trấn Hưng Lợi, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 292, đường 30/4, Ấp 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 136, Lý Thường Kiệt, Ấp 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 35, Quốc lộ 1A, Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Kinh Ngay 1, thị trấn Hưng Lợi, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 263, Quốc Lộ 61B, Ấp 3, Thị trấn Phú lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4333519,
   "Latitude": 105.7442136
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Hòa Đường",
   "address": "Số 170, Lý Thường Kiệt, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 03, Phạm Hùng, Khóm 3, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6093335,
   "Latitude": 105.9795095
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 19, Trần Quang Diệu, Khóm 4, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6006283,
   "Latitude": 105.9694111
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 37, Trần Hưng Đạo, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5982402,
   "Latitude": 105.9719433
 },
 {
   "STT": null,
   "Name": "Nha Khoa Anh Tuấn",
   "address": "Số 19 đường Nguyễn Văn Trỗi, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.604706,
   "Latitude": 105.9758433
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 33/6, đường Calmett, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6104596,
   "Latitude": 105.9710768
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Hưng Hội Tự",
   "address": "Số 81, Lê Lợi, thị trấn Lịch Hội Thượng, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.4862636,
   "Latitude": 106.1463317
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Sản",
   "address": "Số 04, đường Lê Duẩn, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59413099999999,
   "Latitude": 105.972489
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt",
   "address": "Số 04, đường Lê Duẩn, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59413099999999,
   "Latitude": 105.972489
 },
 {
   "STT": null,
   "Name": "Phòng Khám Xuân Diệu",
   "address": "Ấp Châu Thành, thị trấn Lịch Hội Thượng, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.4862636,
   "Latitude": 106.1463317
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 149, Khóm 1, Phường 1, thị xã NGã Năm, Sóc Trăng",
   "Longtitude": 9.5610689,
   "Latitude": 105.6064976
 },
 {
   "STT": null,
   "Name": "Phòng Khám Tư Nhân Chuyên Khoa Nội Tổng Hợp",
   "address": "Ấp Mỹ Hội, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 121/1, ấp Mỹ Thạnh, xã Nhơn Mỹ, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.79352179999999,
   "Latitude": 106.0287512
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 184, Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6009149,
   "Latitude": 105.9699533
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Y Học Cổ Truyền",
   "address": "Số 184, Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6009149,
   "Latitude": 105.9699533
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Ấp An Nhơn, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Minh Ký\"",
   "address": "Ấp Mỹ Huề, xã Nhơn Mỹ, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.79352179999999,
   "Latitude": 106.0287512
 },
 {
   "STT": null,
   "Name": "Phòng Khám \"Lê Hữu Điều\"",
   "address": "Ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 1/10, ấp 9, xã Trinh Phú, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8301841,
   "Latitude": 105.93893
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Chót Dung, xã Kế An, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7718779,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp An Ninh 1, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Bác Sỹ Lê Hoàng Minh",
   "address": "Số 262, ấp Tân Lập B, xã Long Tân, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y \"Viễn Đông\"",
   "address": "Ấp Hòa Khanh, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Khu 1, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Phú Giao, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Khu I, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5010332,
   "Latitude": 105.8494878
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Bắc Dần, xã Phú Mỹ, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5745881,
   "Latitude": 105.9106922
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nhi Và Tiêm Ngừa Dịch Vụ \"Khánh Linh\"",
   "address": " Số 148, đường Mạc Đĩnh Chi, Khóm 3, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6006457,
   "Latitude": 105.9815884
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 246, ấp Phước Bình, xã Mỹ Thuận, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5662234,
   "Latitude": 105.8172881
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 117, ấp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Âp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Mỹ Lợi A, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.60586,
   "Latitude": 105.776198
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 01, đường 30/4, ấp Cầu Đồn, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Âp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Thành, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.76847849999999,
   "Latitude": 105.9758204
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Mang Cá, xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7991216,
   "Latitude": 105.8554513
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 8, ấp An Định, Thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Ninh 1, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 389/8, ấp Phong Hòa, xã Phong Nẫm, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8970152,
   "Latitude": 105.9552997
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 441/9, ấp Phong Hòa, xã Phong Nẫm, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8970152,
   "Latitude": 105.9552997
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, , Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 164A, Hương Lộ 2, Ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7769794,
   "Latitude": 105.9442677
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 377 Tôn Đức Thắng, Khóm 2, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6238263,
   "Latitude": 105.9838575
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 1231/1, ấp Ba Rinh, xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.766667,
   "Latitude": 105.85
 },
 {
   "STT": null,
   "Name": "Phòng Răng \"Nguyễn Hậu\"",
   "address": "Số 14, ấp Phú Thành B, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.70925059999999,
   "Latitude": 105.9652599
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 112, ấp Tân Bình, xã Long Bình, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.53688249999999,
   "Latitude": 105.6353878
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "Số nhà P54/3, ấp 3, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.6221576,
   "Latitude": 106.1169299
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 180, đường 3/2, Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56488959999999,
   "Latitude": 105.5972847
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 16, Ngô Quyền, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5584033,
   "Latitude": 105.990377
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 68, Tỉnh lộ 8, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5591133,
   "Latitude": 105.9923545
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 67/11, đường Tỉnh Lộ 8, Ấp Thạnh lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.559813,
   "Latitude": 105.9909278
 },
 {
   "STT": null,
   "Name": "Phòng Khám Ngoài Giờ Bác Sỹ Phước",
   "address": "Ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 115, Đường 30/4, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 83, ấp Nội Ô, Thị trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Ngoại Tổng Quát",
   "address": "Ấp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Ngoài Giờ Bác Sỹ Trực",
   "address": "Ấp Mỹ An, xã Thiện Mỹ, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67751659999999,
   "Latitude": 105.8583873
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Tá Biên, xã Phú Mỹ, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.575838,
   "Latitude": 105.9099252
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 2/8, Tỉnh lộ 8, ấp Thạnh Lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.559813,
   "Latitude": 105.9909278
 },
 {
   "STT": null,
   "Name": "Phòng Răng Miền Tây",
   "address": "Số 451, Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5879814,
   "Latitude": 105.9755051
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Hòa Khanh, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 06, Đinh Tiên Hoàng, Khóm 2, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6037252,
   "Latitude": 105.9735643
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị \"Phong Hòa Đường\"",
   "address": "Số 411, ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Răng \"Mỹ Ngọc\"",
   "address": "Số 5B, đường Trưng Nhị, Khóm 4, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3252657,
   "Latitude": 105.9817789
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp Khu 2, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Y Sỹ Đóng",
   "address": "Ấp Phương Bình 2, xã Hưng Phú, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6568607,
   "Latitude": 105.7233814
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Cao Phước\"",
   "address": "Ấp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Số 13, ấp Phước Ninh, xã Mỹ Phước huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6034617,
   "Latitude": 105.7116464
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Bác Sỹ Thuận",
   "address": "Ấp Xẻo Gừa, xã Mỹ Hương, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Mỹ Phước\"",
   "address": "Số 10, ấp Tam Sóc B2, xã Mỹ Thuận, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5662234,
   "Latitude": 105.8172881
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Mỹ Phương",
   "address": "Ấp Phú Thành B, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 19, ấp Mỹ Thuận, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Xóm 2, ấp Châu Thành, xã An Ninh, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6106087,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 337, đường Nguyễn Huệ, Ấp 3, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4345248,
   "Latitude": 105.7421523
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 155 Quốc Lộ 1A, ấp Trà Canh A2, xã Thuận Hòa, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.693238,
   "Latitude": 105.9079156
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Văn Đoàn",
   "address": "Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Phượng Ân\"",
   "address": "Số 162A, ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, , Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp Phước An B, xã Mỹ Phước, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6086414,
   "Latitude": 105.7021879
 },
 {
   "STT": null,
   "Name": "Nha Khoa Bảo",
   "address": "Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Nha Khoa Á Đông",
   "address": "Số 236, Điện Biên Phủ, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085254,
   "Latitude": 105.9686399
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Hồng Tuấn Hòa\"",
   "address": "Số 163 Nguyễn Huệ, Khóm 4, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6035419,
   "Latitude": 105.9721027
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 508, đường 30/4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 16, ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.3984115,
   "Latitude": 105.7144082
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Đay Sô, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4573061,
   "Latitude": 105.7986909
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Long Thạnh, xã Tân Long, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52503089999999,
   "Latitude": 105.6588485
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Ấp 1, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.55589279999999,
   "Latitude": 106.1522126
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 33/6, đường Calmett, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6104596,
   "Latitude": 105.9710768
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp số 1, xã Kế An, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.79327769999999,
   "Latitude": 105.8834633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 33, huyện lộ 14, ấp Chợ Cũ, Thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5458374,
   "Latitude": 105.9791851
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 151, Tỉnh lộ 8, ấp Thạnh Lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5564041,
   "Latitude": 105.9980097
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Gia Đình Minh Khang",
   "address": "Số 43, đường Tỉnh Lộ 8, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5592329,
   "Latitude": 105.9885518
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Trường Lâm",
   "address": "Ấp Cổ Cò, xã Ngọc Tố, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết",
   "address": "Số 192 - 194, Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6011002,
   "Latitude": 105.9700207
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Cầu Chùa, xã Kế An, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7718779,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp Chợ, xã Đại Ân 2, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.55589279999999,
   "Latitude": 106.1522126
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Kinh Giữa 1, xã Kế Thành, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7707563,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Bác Sỹ Đức",
   "address": "Ấp An Định, thị trấn Kế sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.77081089999999,
   "Latitude": 105.9763494
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Định, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.77081089999999,
   "Latitude": 105.9763494
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Y Học Gia Đình",
   "address": "Số 84 đường 30/4, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7703173,
   "Latitude": 105.9838277
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Số 05, Ung Công Uẩn, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7688442,
   "Latitude": 105.9859011
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 09, Hương Lộ 2, ấp An Ninh II, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7707972,
   "Latitude": 105.9795473
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi",
   "address": "Số 29, Hương Lộ 2, ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7707972,
   "Latitude": 105.9795473
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Ninh 2, xã An lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Nhơn, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Lê Thị Hà",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Hòa An, xã Xuân Hòa, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.87338239999999,
   "Latitude": 105.9019276
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 7, xã Ba Trinh, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.821893,
   "Latitude": 105.8877494
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp số 1, xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.79327769999999,
   "Latitude": 105.8834633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Số 07, Ung Công Uẩn, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7688442,
   "Latitude": 105.9859011
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 70, Tỉnh Lộ 934, ấp Đại Nôn, xã Liêu Tú, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.5591023,
   "Latitude": 105.9923756
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Thần Kinh Bác Sỹ Lâm Ngọc Hùng",
   "address": "Số 19, đường Phú Lợi, Khóm 2, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5962553,
   "Latitude": 105.9677951
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 43 - 45, Trần Bình Trọng, Khóm 1, Phường 2, thành phố sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5975887,
   "Latitude": 105.9681002
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa Bác Sỹ Nguyễn Thu Nghiêm",
   "address": "Số 98, Lê Văn Tám, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5923102,
   "Latitude": 105.9739416
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 271, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59308989999999,
   "Latitude": 105.9689815
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 23/5 đường Đề Thám, Khóm 4, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6063104,
   "Latitude": 105.9707577
 },
 {
   "STT": null,
   "Name": "Phòng Khám Tư Nhân Trường Sinh",
   "address": "Số 251, Lê Hồng Phong, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.593005,
   "Latitude": 105.974519
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 447, Nguyễn Huệ, Khóm 4, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5997184,
   "Latitude": 105.9793564
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Vương Ngọc Thắng",
   "address": "Số 18 Ngô Gia Tự, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6068386,
   "Latitude": 105.974776
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 29, Đường Nguyễn Văn Trỗi, Khóm 1, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.604706,
   "Latitude": 105.9758433
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 30, đường Sơn Đê, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085491,
   "Latitude": 105.9738324
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 61, Lê Hồng Phong, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59869739999999,
   "Latitude": 105.9737088
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 35 Võ Thị Sáu, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5947445,
   "Latitude": 105.9729085
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Số 34, đường 30/4, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 34, đường 30/4, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 06A, đường 3/2, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 30B, Trần Hưng Đạo, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phương Bình 1, xã Hưng Phú, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6566069,
   "Latitude": 105.7237891
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 3, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56397589999999,
   "Latitude": 105.6037894
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Mỹ Thành, xã Mỹ Quới, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.46452399999999,
   "Latitude": 105.5720543
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Kinh Ngay 1, thị trấn Hưng Lợi, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp Nàng Rền, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Tiêm (Chích), Thay Băng",
   "address": "Số 32, Quốc Lộ 1A, ấp Xa mau 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Hồng Mơ",
   "address": "Ấp B2, xã Thạnh Tân, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.5121067,
   "Latitude": 105.7057792
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Vân Đô",
   "address": "Số 190, ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52151709999999,
   "Latitude": 105.6177942
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Ngoại",
   "address": "Ấp 2, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.60925389999999,
   "Latitude": 106.135192
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Thu Lài\"",
   "address": "Số 188, ấp Kinh Mới, xã Vĩnh Hiệp, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3875385,
   "Latitude": 105.9582372
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Đức An\"",
   "address": "Số 179D đường Nguyễn Trãi, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.326721,
   "Latitude": 105.9816005
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 191K, Đường 30/4, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3293404,
   "Latitude": 105.9801748
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Tiêm, Thay Băng \"Xuân Hoa\"",
   "address": "Số 190A, Đường 30/4, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3241857,
   "Latitude": 105.9801782
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Mỹ Hạnh\"",
   "address": "Số 59E, Khóm 1, đường Nguyễn Huệ, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 1, Trưng Nhị, Khóm 4, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3251706,
   "Latitude": 105.9817903
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Nhi \"Bạch Yến\"",
   "address": "Số 209H, đường 30/4, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3293404,
   "Latitude": 105.9801748
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Mền Cán\"",
   "address": "Số 12, ấp Âu Thọ, xã Vĩnh Hải, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3788328,
   "Latitude": 106.1463317
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Tâm Đức\"",
   "address": "Ấp Ca Lạc A, xã Lạc Hòa, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3821101,
   "Latitude": 106.0757749
 },
 {
   "STT": null,
   "Name": "Phòng Khám \"Bác Sỹ Kiêm Đại\"",
   "address": "Số 170, Khóm Xẻo Me, Phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Cẩm Niên\"",
   "address": "Số 105, Khóm Xẻo Me, Phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà \"Trinh Trang\"",
   "address": "Ấp Mỹ Lợi C, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.635541,
   "Latitude": 105.811547
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 98, ấp Phú Bình, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694783,
   "Latitude": 105.9621262
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Tập, xã Thiện Mỹ, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67751659999999,
   "Latitude": 105.8583873
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Xây Đá B, xã Hồ Đắc Kiện, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.7150088,
   "Latitude": 105.8642593
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Trương Hiền, xã Thạnh Trị, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4639086,
   "Latitude": 105.6999122
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.43187249999999,
   "Latitude": 105.7505734
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"An Phước\"",
   "address": "Số 45, Nguyễn Huệ, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 53, đường Dương Minh Quan, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5876503,
   "Latitude": 105.9677563
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Cứ Mạnh, xã Xuân Hòa, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.85943629999999,
   "Latitude": 105.8922172
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị \"Tuệ Tĩnh Đường Ngọc Tâm\"",
   "address": "Số 02, Khu 1, ấp Phước An, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị \"Đông Phương\"",
   "address": "Số 263, Khu II, ấp Phước Lợi, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "Số 09, Yết Kiêu, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085309,
   "Latitude": 105.9702302
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Ninh 2, xã An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Trà quýt A, Thị trấn Châu Thành, huyện Châu thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 66, Quốc Lộ IA, Ấp Đại Chí, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.543893,
   "Latitude": 105.917459
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 15, tỉnh lộ 8, ấp Thạnh Lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.559813,
   "Latitude": 105.9909278
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 196/53, Tôn Đức Thắng, Khóm 1, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6093361,
   "Latitude": 105.977294
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 117/2, Phạm Hùng, Khóm 3, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.610145,
   "Latitude": 105.9822748
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Sơn Phương\"",
   "address": "Số 414 Tôn Đức Thắng, Khóm 2, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6185306,
   "Latitude": 105.9817887
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội",
   "address": "Số 76, Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5971709,
   "Latitude": 105.967971
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Tăng Vũ",
   "address": "Số 374 Tôn Đức Thắng, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6171211,
   "Latitude": 105.981135
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà \"Bạch Phượng\"",
   "address": "Số 151, đường 3/2, Ấp 1, Thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.563153,
   "Latitude": 105.5952
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Hải",
   "address": "Số 08, ấp Bố Liên 1, xã Thuận Hưng, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.59910399999999,
   "Latitude": 105.8055471
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73177029999999,
   "Latitude": 106.0696859
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Bác Sỹ Huỳnh Kim Thúy",
   "address": "Số 211L đường 30/4, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3293404,
   "Latitude": 105.9801748
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 76A, Đinh Tiên Hoàng, Khóm 3, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.6054617,
   "Latitude": 105.9740253
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 14, Quốc lộ 1A, ấp Đại Chí, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số nhà A5-6, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5601205,
   "Latitude": 105.9886104
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Cổ Cò, xã Ngọc Tố, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Bình",
   "address": "Số 121, Ấp Lương Văn Hoàng, xã Ngọc Tố, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Răng \"Đăng Khoa\"",
   "address": "Ấp Khu 1, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Trung Tâm Giám Định Y Khoa",
   "address": "Số 4, đường Pasteur, Khóm 2, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6103898,
   "Latitude": 105.9795958
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị \"Điện Thờ Phật Mẫu\"",
   "address": "Số 250, đường 30-4, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 189, đường Sóc Vồ, Khóm 4, Phường 7, thành phố Sóc Trăng., Sóc Trăng",
   "Longtitude": 9.6282905,
   "Latitude": 105.9512698
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Kim Phượng\"",
   "address": "Số 123 C, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.322198,
   "Latitude": 105.979003
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y",
   "address": "Số 15, đường Dã Tượng, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60918459999999,
   "Latitude": 105.9712544
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Tuấn",
   "address": "Số 2 C, đường Pasteur, Khóm 2, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6103898,
   "Latitude": 105.9795958
 },
 {
   "STT": null,
   "Name": "Phòng Khám Khai Minh",
   "address": "Số 12, đường 30-4, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Võ Văn Bông",
   "address": "Số 89, đường Bà Triệu, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6090353,
   "Latitude": 105.9789227
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y Chùa Phật Học",
   "address": "Số 138, đường Trần Hưng Đạo, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6020016,
   "Latitude": 105.9739827
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 3, Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4340376,
   "Latitude": 105.7426157
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Đào Viên, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tân Lập B, xã Long Tân, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.6048121,
   "Latitude": 105.6485653
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": " Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.43187249999999,
   "Latitude": 105.7505734
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y Hội Xuân",
   "address": "Số 62, đường Đinh Tiên Hoàng, Khóm 2, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6047806,
   "Latitude": 105.9738211
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Trương Thái Hùng",
   "address": "Ấp Phú Thành B, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đa Khoa Bác Sỹ Hên",
   "address": "Số 144, đường Nguyễn Văn Linh, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60192569999999,
   "Latitude": 105.9649176
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Răng \"Bảo Ngân\"",
   "address": "Số 6, đường Lê Lai, Khóm V, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3241857,
   "Latitude": 105.9801782
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": " Ấp Long Thành, xã Tân Long, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52503089999999,
   "Latitude": 105.6588485
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 115G, đường Nguyễn Huệ, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Phòng Xét Nghiệm Y Khoa",
   "address": "Số 472, đường 30 tháng 4, Khóm 1, Phường 3, Thành Phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": null,
   "Name": "Phòng Nha Quốc Toại",
   "address": "Số 261, đường Lê Hồng Phong, Khóm 4, Phường 3, Thành Phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.593005,
   "Latitude": 105.974519
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phú Bình, xã Phú Tâm, huyện Châu Thành, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.694783,
   "Latitude": 105.9621262
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Xẽo Gừa, xã Mỹ Hương, huyện Mỹ Tú, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Nhi ",
   "address": "Số 8, đường Triệu Nương, ấp Châu Thành, thị trấn mỹ Xuyên, huyện Mỹ Xuyên, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5594438,
   "Latitude": 105.9853184
 },
 {
   "STT": null,
   "Name": "Phòng Xét Nghiệm",
   "address": "Số 50, đường Pasteur, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6103898,
   "Latitude": 105.9795958
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhân Đạo",
   "address": "Ấp 8, xã Ba Trinh, huyện Kế Sách, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.821893,
   "Latitude": 105.8877494
 },
 {
   "STT": null,
   "Name": "Phòng Khám Lý Ngọc Tú",
   "address": "Số 319 Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59212159999999,
   "Latitude": 105.9684844
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 55, ấp Đại Thành, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Sản Phụ Khoa",
   "address": "Số 542, ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, , Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu",
   "address": "Số 166 Lê Hồng Phong, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5620228,
   "Latitude": 105.9805955
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 155, Nguyễn Trung Trực, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5996419,
   "Latitude": 105.9698964
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Á Đông",
   "address": "Số 236, Điện Biên Phủ, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085254,
   "Latitude": 105.9686399
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 56, Tỉnh lộ 8, xã Tài Văn, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.55917949999999,
   "Latitude": 105.9922279
 },
 // {
 //   "STT": null,
 //   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
 //   "address": "Số 248, ấp Thạnh An 3, xã Thạnh Thới Thuận, huyện Trần Đề, Sóc Trăng",
 //   "Longtitude": 9.4702676,
 //   "Latitude": 106.0287512
 // },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phước Ninh, xã Mỹ Phước, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5907953,
   "Latitude": 105.7405114
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 12, Lý Thường Kiệt, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5767891,
   "Latitude": 105.9959513
 },
 // {
 //   "STT": null,
 //   "Name": "Phòng Răng Pho",
 //   "address": "Ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
 //   "Longtitude": 9.73177029999999,
 //   "Latitude": 106.0696859
 // },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sĩ Hùng - Bác Sĩ Thảo",
   "address": "Số 455, Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5878564,
   "Latitude": 105.9755929
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội",
   "address": "Số 244, Lê Duẩn, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5931145,
   "Latitude": 105.9805774
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 132 đường Bà Triệu, Khóm 2, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6090353,
   "Latitude": 105.9789227
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội",
   "address": "số 488, đường Nguyễn Huệ, Khóm 2, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5981116,
   "Latitude": 105.9815891
 },
 {
   "STT": null,
   "Name": "Phòng Khám Thanh Tâm",
   "address": "Số 83, Nguyễn Huệ, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 89, ấp Đay Sô, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4482509,
   "Latitude": 105.7951601
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đa Khoa Khu Vực Xã Mỹ Phước",
   "address": "Ấp Phước Ninh, xã Mỹ Phước, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5907953,
   "Latitude": 105.7405114
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Ngoại",
   "address": "Khu 4, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 445, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58925689999999,
   "Latitude": 105.9666008
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": " Ấp Kinh Ngay 1, thị trấn Hưng Lợi, huyện Thạnh Trị, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Tiêm Thuốc Thay Băng",
   "address": "Ấp Kinh Ngay 1, thị trấn Hưng Lợi, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 299/2, ấp An Hòa, xã An Lạc Tây, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.844109,
   "Latitude": 105.9876149
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 265, Tôn Đức Thắng, Khóm 2, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6176593,
   "Latitude": 105.9812075
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phú Ninh, xã An Ninh, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6188126,
   "Latitude": 105.9433346
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 156, ấp Tâm Thọ, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đại Tâm",
   "address": "Ấp Tâm Kiên, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.43187249999999,
   "Latitude": 105.7505734
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "Số 255, Quốc Lộ 1A, ấp An Trạch, xã An Hiệp, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6505713,
   "Latitude": 105.9388876
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội",
   "address": "Số 209, đường 30 tháng 4, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 135, Khóm 5, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.322198,
   "Latitude": 105.979003
 },
 {
   "STT": null,
   "Name": "Phòng Răng Nguyên Thông",
   "address": "Số 163D, đường Nguyễn Huệ, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 1/1, Lê Lai, Khóm 6, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.32593009999999,
   "Latitude": 105.9824359
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 04, Tỉnh Lộ 934, ấp Châu thành, thị trấn Mỹ Xuyên, huyện Mỹ xuyên, Sóc Trăng",
   "Longtitude": 9.5595289,
   "Latitude": 105.9898956
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 347, đường Phú Lợi, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.595995,
   "Latitude": 105.9690456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 843, đường Phạm Hùng, Khóm 7, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5987894,
   "Latitude": 105.9721725
 },
 {
   "STT": null,
   "Name": "Phòng Răng Hữu Nghị",
   "address": "Số 38-40 Nguyễn Thị Minh Khai, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60058729999999,
   "Latitude": 105.9752864
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y Trần Phước Đường",
   "address": "149, Hai Bà Trưng, Khóm 2, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6038217,
   "Latitude": 105.9745083
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 73, Phường 2, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Nha Khoa Bửu Lâm",
   "address": "Số 76 đường Đồng Khởi, Khóm 1, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6030753,
   "Latitude": 105.9759916
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Vũ Đức Lành",
   "address": "Ấp Đông Hải, xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.77867779999999,
   "Latitude": 105.8877494
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Vinh Nghĩa Đường\"",
   "address": "Số 347, Mai Thanh Thế, Ấp 1, thị trấn Ngã Năm, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56191529999999,
   "Latitude": 105.5949271
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Vinh Thọ Đường\"",
   "address": "Số 224, ấp Nội Ô, Thị Trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Vinh Hòa Đường\"",
   "address": "Ấp 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt",
   "address": "14 Lê Hồng Phong, Khóm 7, Phường 3, thành phố Sóc trăng, Sóc Trăng",
   "Longtitude": 9.59906799999999,
   "Latitude": 105.9734797
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Tập, xã Thiện Mỹ, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67751659999999,
   "Latitude": 105.8583873
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Ửng",
   "address": "Số 420/3, đường Tôn Đức Thắng, Khóm 2, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6176164,
   "Latitude": 105.9808256
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Vạn Quang Vũ\"",
   "address": "Số 379, ấp Trà Quít A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Sơn Con",
   "address": "Ấp Tiếp Nhựt, xã Viên An, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.51729469999999,
   "Latitude": 106.0581397
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "ấp Long Thạnh, xã Tân Long, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52503089999999,
   "Latitude": 105.6588485
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Vạn An Đường\"",
   "address": "Lô 7-8 Nhà lồng chợ Mỹ Hương, xã Mỹ Hương, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Thuận Hòa",
   "address": "Số 239, ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 19A, ấp Mỹ Thành, xã Mỹ Quới, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.453873,
   "Latitude": 105.5650239
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 387, ấp Cổ Cò, xã Ngọc Tố, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Nha Khoa Việt Mỹ",
   "address": "Số 09, đường Mai Thanh Thế, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5628402,
   "Latitude": 105.5963065
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Yhct Phước Thiện \"Hưng Khánh Tự\"",
   "address": "Số 91, Lê Lợi, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5601205,
   "Latitude": 105.9886104
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 57, Ấp Vĩnh Thuận, xã Vĩnh Quới, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5724495,
   "Latitude": 105.5650239
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y \"Vĩnh Tế Sanh\"",
   "address": "Số 387, Võ Thị Sáu, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5610689,
   "Latitude": 105.6064976
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 25-LK25, đường số 8, Khu Dân cư 5A, Khóm 4, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5894025,
   "Latitude": 105.9635025
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi",
   "address": "Số 172 Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60066,
   "Latitude": 105.9698643
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 61 Nguyễn Văn Thêm, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6003796,
   "Latitude": 105.9735845
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 202, đường 3/2, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.56488959999999,
   "Latitude": 105.5972847
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 135A, Nguyễn Huệ, Khóm 1, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3234619,
   "Latitude": 105.974626
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh \"Ngọc Liễu\"",
   "address": "Số 239, ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Khóm Xẽo Me, phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Trần Việt Thắng",
   "address": "Khóm Vĩnh Tiền, Phường 3, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.51234019999999,
   "Latitude": 105.5826123
 },
 {
   "STT": null,
   "Name": "Nha Khoa Phú Lợi",
   "address": "Số 34, Phú Lợi, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5961444,
   "Latitude": 105.9687649
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Trà Quýt A, Thị trấn Châu Thành, huyện Châu thành, Sóc Trăng",
   "Longtitude": 9.7102902,
   "Latitude": 105.9002191
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Số 08, Lê Hồng Phong, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5991977,
   "Latitude": 105.9735217
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi ",
   "address": "Số 88, ấp Đại Chí, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5447211,
   "Latitude": 105.928426
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 32, Lê Lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5605122,
   "Latitude": 105.9877727
 },
 {
   "STT": null,
   "Name": "Phòng Khám Minh Hòa",
   "address": "Số 25, Lê Lợi, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60774979999999,
   "Latitude": 105.9753073
 },
 {
   "STT": null,
   "Name": "Nha Khoa Diệp Huy",
   "address": "Số 2/12, đường Bùi Viện, Khóm 1, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60484299999999,
   "Latitude": 105.978108
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 3, đường Bùi Viện, Khóm 1, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6048068,
   "Latitude": 105.9773504
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp An Tập, xã Thiện Mỹ, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67751659999999,
   "Latitude": 105.8583873
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 81A, đường Đề Thám, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3241857,
   "Latitude": 105.9801782
 },
 {
   "STT": null,
   "Name": "Trung Tâm Chăm Sóc Sức Khỏe Sinh Sản",
   "address": "Số 01, Lê Lai, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60686209999999,
   "Latitude": 105.9751891
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y \"Hưng Lạc Tự\"",
   "address": "Ấp An Thạnh, xã An Lạc Tây, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.844109,
   "Latitude": 105.9876149
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 79, Khóm 1, Phường 2, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.57469869999999,
   "Latitude": 105.9575561
 },
 {
   "STT": null,
   "Name": "Nha Khoa Đại Phúc",
   "address": "Hương lộ 6, ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh",
   "address": "Số 264, đường Lý Thường Kiệt, Khóm 1, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5838579,
   "Latitude": 106.0062192
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Ấp Bưng Sa, xã Viên An, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.49978,
   "Latitude": 106.0875326
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Mỹ Hòa, xã Mỹ Tú, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.60586,
   "Latitude": 105.776198
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Khu 2, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Ninh 1, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Thành, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.76847849999999,
   "Latitude": 105.9758204
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Bưng Tróp A, xã An Hiệp, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6471675,
   "Latitude": 105.9345238
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi",
   "address": "4+62/10/13, Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59777439999999,
   "Latitude": 105.9736941
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nhi",
   "address": "Số 03, đường Lê Lai, Khóm 3, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3169364,
   "Latitude": 105.9834711
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Ngoại",
   "address": "Số 154, ấp Chắc Tưng, xã Tài Văn, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5427384,
   "Latitude": 106.0233852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phú Thành B, xã Phú Tâm, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Phước Hòa, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67307999999999,
   "Latitude": 105.9523622
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 743/1, ấp Mang Cá, xã Đại Hải, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7991216,
   "Latitude": 105.8554513
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Như Ý\"",
   "address": "Đường Mai Thanh Thế, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5640377,
   "Latitude": 105.597087
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Số 460, đường Lý Thường Kiệt, Phường 4, thành phố Sóc trăng, Sóc Trăng",
   "Longtitude": 9.5838579,
   "Latitude": 106.0062192
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Số 790, ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.78557339999999,
   "Latitude": 106.0816536
 },
 {
   "STT": null,
   "Name": "Phòng Răng KTV Nguyễn Văn Ngoan",
   "address": "Ấp An Ninh 2, thị trấn An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Mỹ Thành, xã Mỹ Quới, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.46452399999999,
   "Latitude": 105.5720543
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 523, đường Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.593005,
   "Latitude": 105.974519
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi",
   "address": "Ấp Mỹ Thạnh, xã Mỹ Quới, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.46452399999999,
   "Latitude": 105.5720543
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tân Lập, xã Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.583,
   "Latitude": 106.133
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Khu vực II, ấp An Ninh 2, thị trấn An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tân Bình, xã Long Bình, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.53688249999999,
   "Latitude": 105.6353878
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Sô La 1, xã Tham Đôn, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5110497,
   "Latitude": 105.9251353
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi",
   "address": "Ấp Tân Phước, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5598517,
   "Latitude": 105.944824
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 141, ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Khóm 2, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.563124,
   "Latitude": 105.595363
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 156, đường 3 tháng 2, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.57469869999999,
   "Latitude": 105.9575561
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "ấp Nguyễn Công Minh B, xã An Thạnh Đông, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6505306,
   "Latitude": 106.199266
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại",
   "address": "Số 149, Xô Viết Nghệ Tĩnh, Khóm 4, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6084743,
   "Latitude": 105.972809
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Lâm Thương",
   "address": "Số 209, Khóm Sở Tại A, phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bênh Sơn Thị Mai",
   "address": "Số 18, Khóm Vĩnh Thành, Phường Vĩnh Phước, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Ánh Như\"",
   "address": "Số 67, Ấp Cảng, thị trấn Trần Đề, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.484046,
   "Latitude": 106.1933837
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 174, Ấp Vĩnh Tiền, xã Vĩnh Biên, huyện Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5383783,
   "Latitude": 105.5695099
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Hưng Bình Tự\"",
   "address": "Ấp Tân Bình, xã Long Bình, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.53688249999999,
   "Latitude": 105.6353878
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Ấp 3, Thị trấn Phú lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4241817,
   "Latitude": 105.7388798
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Long Hòa, xã Gia Hòa 1, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4297548,
   "Latitude": 105.8583873
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 19, Văn Ngọc Chính, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5854185,
   "Latitude": 105.9754292
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Minh Cảnh",
   "address": "Ấp Hòa Trực, xã Hòa Tú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4262895,
   "Latitude": 105.8743369
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 157, tỉnh lộ 934, ấp Thạnh Lợi, thị trấn Mỹ Xuyện, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.55634729999999,
   "Latitude": 105.9981463
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 94, ấp Giồng Có, xã Tham Đôn, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.46551109999999,
   "Latitude": 105.8994956
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Ca Lạc A, xã Lạc Hòa, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3821101,
   "Latitude": 106.0757749
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Số 43, Ấp Kinh Mới, xã An Ninh, huyện Châu Thành , Sóc Trăng",
   "Longtitude": 9.6106087,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 29A, đường 934, ấp Chợ Cũ, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5580359,
   "Latitude": 105.9793076
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 190, ấp Trà Bết, xã Tham Đôn, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.517683,
   "Latitude": 105.9347384
 },
 {
   "STT": null,
   "Name": "Nha Khoa Hoàng Anh",
   "address": "Số 245 Trần Hưng Đạo, Phường 3, thành phố Sóc Trăng , Sóc Trăng",
   "Longtitude": 9.59358149999999,
   "Latitude": 105.9693359
 },
 {
   "STT": null,
   "Name": "Phòng Khám Da Liễu Nguyễn Hoàng Nhật",
   "address": "Số 176, Trương Công Định, Khóm 6, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60375859999999,
   "Latitude": 105.9660736
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Hữu Nghị Ii",
   "address": "Số 59, đường 30 tháng 4, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "Số 04 Trần Văn Bảy, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.590302,
   "Latitude": 105.976084
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 39A, Lê Lợi, Khóm 2, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.330204,
   "Latitude": 105.9788011
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 171, ấp Tâm Thọ, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": null,
   "Name": "Nha Khoa Toàn Mỹ",
   "address": "Số 265, Phú Lợi, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5962553,
   "Latitude": 105.9677951
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 27 Nguyễn Văn Thêm, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6015565,
   "Latitude": 105.9743756
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sĩ Vũ",
   "address": "Số 124, ấp Dương Kiển, xã Hòa Tú 2, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4496457,
   "Latitude": 105.8936224
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đa Khoa Bác Sĩ Năng",
   "address": "Số 419, ấp Cổ Cò, xã Ngọc Tố, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Số 08, Triệu Nương, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5596278,
   "Latitude": 105.9898065
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thái Nguyên",
   "address": "Số 140, ấp Phước An, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.694912,
   "Latitude": 105.9052259
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tân Thành, xã Long Hưng, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6967403,
   "Latitude": 105.7938069
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Thịnh",
   "address": "Số 176B, đường Mạc Đĩnh Chi, Khóm 4, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59548519999999,
   "Latitude": 105.9954599
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đa Khoa Vạn Phúc",
   "address": "Số17 - 19, đường Lê Lợi, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6085238,
   "Latitude": 105.9738761
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp 2, thị trấn Long Phú, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.60925389999999,
   "Latitude": 106.135192
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 18 Triệu Nương, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5595652,
   "Latitude": 105.9895658
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Từ Thiện Phước Lâm",
   "address": "Số 70, ấp Chợ Cũ, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5458374,
   "Latitude": 105.9791851
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 49, ấp Khu 2, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Mỹ Khánh A, xã Long Hưng, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.6967403,
   "Latitude": 105.7938069
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 85, ấp Tam Sóc B1, xã Mỹ Thuận, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5662234,
   "Latitude": 105.8172881
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Thịnh Đường",
   "address": "Ấp Mỹ Thuận, Thị trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.635541,
   "Latitude": 105.811547
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Ngọc Định",
   "address": "Khóm Cà Lăng A, Phường 2, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3132583,
   "Latitude": 106.0118549
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đa Khoa Khu Vực Đại Ngãi",
   "address": "Ấp Ngãi Hội 2, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Nội Khoa",
   "address": "Số 365, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59108129999999,
   "Latitude": 105.9677655
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Trung Hòa, xã Tuân Tức, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.488951,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tâm Lộc, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5428648,
   "Latitude": 105.9141793
 },
 {
   "STT": null,
   "Name": "Nha Khoa Việt Mỹ",
   "address": "Số 182/2, đường 30 tháng 4, ấp 1, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Ấp An Nghiệp, xã An Thạnh 3, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.5667077,
   "Latitude": 106.2639825
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Ấp Phước An, xã Phú Tâm, huyện Châu thành, Sóc Trăng",
   "Longtitude": 9.67307999999999,
   "Latitude": 105.9523622
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 08, đường Phạm Hùng, Khóm 3, Phường 8, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.61430839999999,
   "Latitude": 106.0007548
 },
 {
   "STT": null,
   "Name": "Nha Khoa Nụ Cười Sài Gòn",
   "address": "Số 134 đường Hai Bà Trưng, Khóm 1, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6038217,
   "Latitude": 105.9745083
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "29 Nguyễn Văn Trỗi, Khóm 1, Phường 1, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6049053,
   "Latitude": 105.9757752
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Cảng, thị trấn Trần Đề, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.484046,
   "Latitude": 106.1933837
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Lê Văn Đôi\"",
   "address": "Số 657, ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": null,
   "Name": "Phòng Khám Thiện Tâm",
   "address": "Ấp An Thành, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.76847849999999,
   "Latitude": 105.9758204
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Thanh Hùng",
   "address": "Số 35, ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sĩ Hồ Hoàng Vũ",
   "address": "Khóm 1, Phường 2, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Thiện Hưng Giác Tự",
   "address": "Đường Nguyễn Trung Trực, Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5601298,
   "Latitude": 105.6012096
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Vĩnh Thuận, xã Vĩnh Quới, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.58293,
   "Latitude": 105.558335
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nôi Tổng Hợp",
   "address": "Số 40, ấp Tiên Cường 1, xã Thạnh Thới An, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.4935651,
   "Latitude": 106.0060112
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Phước Thuận, xã Phú Tân, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.67307999999999,
   "Latitude": 105.9523622
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Số 28 Ấp Trà Sết, xã Vĩnh Hải, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3788328,
   "Latitude": 106.1463317
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Mỹ",
   "address": "Ấp Mỹ Thuận, thị trấn Huỳnh Hữu Nghĩa, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.635541,
   "Latitude": 105.811547
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa",
   "address": "Số 246A, Tôn Đức Thắng, Khóm 1, Phường 5, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.61355399999999,
   "Latitude": 105.9793911
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vinh Xuân Đường",
   "address": "Số 09, Tỉnh Lộ 934, ấp Thạnh Lợi, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.557592,
   "Latitude": 105.9813427
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Lâm",
   "address": "Số 76, ấp Chợ Cũ, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5458374,
   "Latitude": 105.9791851
 },
 {
   "STT": null,
   "Name": "Phòng Khám Đông Y \"Bách Phương\"",
   "address": "Số 281, Mạc Đĩnh Chi, Khóm 4, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59672069999999,
   "Latitude": 105.9922139
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Khu I, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5010332,
   "Latitude": 105.8494878
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Ấp Ninh Thới, xã Thới An Hội, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 // {
 //   "STT": null,
 //   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền \"Thiên An Đường\"",
 //   "address": "Số 310, đường 30 tháng 4, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
 //   "Longtitude": 10.021629,
 //   "Latitude": 105.7702742
 // },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Khóm 1, Phường 1, thị xã Ngã Năm, tỉnh Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5610689,
   "Latitude": 105.6064976
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Tam Sóc A, xã Mỹ Thuận, huyện Mỹ Tú, Sóc Trăng",
   "Longtitude": 9.5662234,
   "Latitude": 105.8172881
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Áp Châu Thành, thị trấn Lịch Hội Thượng, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.4862636,
   "Latitude": 106.1463317
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Thanh Tùng\"",
   "address": "Số 02, Dã Tượng, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6094292,
   "Latitude": 105.9711394
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị \"Ngân Nghĩa Đường\"",
   "address": "Số 138, Khóm 1, Phường 2, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Khóm 1, Phường 2, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 24 Trần Hưng Đạo, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5984112,
   "Latitude": 105.9720677
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Ấp Chợ, thị trấn Cù Lao Dung, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Bình Hòa, xã Gia Hòa 2, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.39964099999999,
   "Latitude": 105.8114175
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Nha Khoa \"Hồng Nguyên\"",
   "address": "Số 451, ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trí Đức Sông Đinh",
   "address": "Số 849, Lý Thường Kiệt, Khóm 6, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6072816,
   "Latitude": 106.0089541
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Mã Tịch",
   "address": "150 Trần Hưng Đạo, Khóm 3, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3223405,
   "Latitude": 105.9801782
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Từ Thiện Xuân Hương",
   "address": "Số 27, Ung Công Uẩn, ấp An Ninh 1, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7687055,
   "Latitude": 105.9840314
 },
 {
   "STT": null,
   "Name": "Phòng Chụp X - Quang",
   "address": "Số 66, đường Trương Công Định, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6048981,
   "Latitude": 105.9640638
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "Số 66, Quốc Lộ I, Ấp Đại Chí, xã Đại Tâm, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.543893,
   "Latitude": 105.917459
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 439, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5893971,
   "Latitude": 105.9667206
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 417, ấp Đắc Lực, xã Hồ Đắc Kiện, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.7150088,
   "Latitude": 105.8642593
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Thiện Hưng Ân Tự",
   "address": "Ấp Chợ, xã Đại Ân 2, huyện Trần Đề, Sóc Trăng",
   "Longtitude": 9.55589279999999,
   "Latitude": 106.1522126
 },

 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Ấp Khu 1, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Thiện Hưng Thạnh",
   "address": "Ấp An Phú A, xã An Thạnh Tây, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.6862394,
   "Latitude": 106.1286901
 },
 {
   "STT": null,
   "Name": "Phòng Khám Minh Đức",
   "address": "Số 41 Lê Lợi, Khóm 3, Phường 1, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.3223405,
   "Latitude": 105.9801782
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Nha Khoa \"Xuân Mai\"",
   "address": "Số 278/6, Trương Công Định, Khóm 5, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.602223,
   "Latitude": 105.9709172
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phước Thiện \"Hưng Trung Tự\"",
   "address": "Số 77, ấp Tân Trung, xã Long Bình, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.53688249999999,
   "Latitude": 105.6353878
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Nha Khoa \"Thu Sương\"",
   "address": "Số 96 Trần Bình Trọng, Khóm 1, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.60178479999999,
   "Latitude": 105.9723464
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 12A, đường 30 tháng 4, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng Bác Sỹ Nguyễn Thông Sáng",
   "address": "Ấp Trương Hiền, xã Thạnh Trị, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4639086,
   "Latitude": 105.6999122
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vĩnh Đông",
   "address": "Khóm Vĩnh Tiền, Phường 3, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.51234019999999,
   "Latitude": 105.5826123
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Ninh 2, thị trấn An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Ninh, thị trấn An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khánh Phúc Đường",
   "address": "Số 31 Lê Văn Tám, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5933237,
   "Latitude": 105.9726888
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền (Từ Thiện)",
   "address": "Số 542/107/19, Nguyễn Huệ, Khóm 4, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5990482,
   "Latitude": 105.9798471
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Hoàng Lâm",
   "address": "Ấp 2, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.43187249999999,
   "Latitude": 105.7505734
 },
 {
   "STT": null,
   "Name": "Phòng Khám Hùng Vương (Chuyên Khoa Tai Mũi Họng)",
   "address": "Số 31 Hùng Vương, Khóm 2, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6113447,
   "Latitude": 105.9697367
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Từ Thiện",
   "address": "Ấp Lai Hòa, xã Lai Hòa, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.30744569999999,
   "Latitude": 105.8410406
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp Khu 1, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bệnh Bác Sỹ Đặng Minh Hiền",
   "address": "Số 317 Lê Duẩn, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.59351,
   "Latitude": 105.9803985
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Phương Nga",
   "address": "Ấp Kinh Ngay 1, thị trấn Hưng Lợi, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 24 Nam Kỳ Khởi Nghĩa, Khóm 4, Phường 7, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6074514,
   "Latitude": 105.9598356
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Ấp An Định, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.77081089999999,
   "Latitude": 105.9763494
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Từ Thiện Hưng Hòa Tự",
   "address": "Số 02 Mạc Đĩnh Chi, Khóm 1, Phường 9, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6038874,
   "Latitude": 105.976887
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Thành Đạt Sài Gòn",
   "address": "Số 495, ấp An Nghiệp, xã An Thạnh 3, huyện Cù Lao Dung, Sóc Trăng",
   "Longtitude": 9.5667077,
   "Latitude": 106.2639825
 },
 {
   "STT": null,
   "Name": "Nha Khoa Lâm Phát",
   "address": "Số 441, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5893866,
   "Latitude": 105.9666216
 },
 {
   "STT": null,
   "Name": "Phòng Khám Tư Nhân Chuyên Khoa Nội Nhi",
   "address": "Số 31, ấp An Ninh 2, thị trấn Kế sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nha Khoa Bác Sỹ Thắm",
   "address": "Số 87, Lê Lợi, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.56143769999999,
   "Latitude": 105.9855319
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Hoàn Mỹ",
   "address": "Số 286 Lê Hồng Phong, Khóm 5, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58963189999999,
   "Latitude": 105.9750411
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Số 244/1, đường Phú Lợi, Khóm 3, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.595995,
   "Latitude": 105.9690456
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 347, Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5915834,
   "Latitude": 105.9680657
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "Số 06 đường Lê Hồng Phong, Khóm 7, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5987894,
   "Latitude": 105.9721725
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phục Hồi Chức Năng",
   "address": "Số 516, Nguyễn Văn Linh, Khóm 2, Phường 2, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58904259999999,
   "Latitude": 105.9656156
 },
 {
   "STT": null,
   "Name": "Phòng Khám Sản Phụ Khoa Bác Sỹ Thoa",
   "address": "Số 473 đường Trần Hưng Đạo, Khóm 8, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.58409829999999,
   "Latitude": 105.9631303
 },
 {
   "STT": null,
   "Name": "Phòng Khám Bác Sỹ Cao Cẩm Thành",
   "address": "Số 57, Đường Số 1, Khu dân cư 586, Trần Hưng Đạo, Khóm 2, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5877268,
   "Latitude": 105.9647039
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Răng Miệng",
   "address": "Số 549, đường 30 tháng 4, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Lê Tâm",
   "address": "Số 198 Lê Hồng Phong, Khóm 4, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5920975,
   "Latitude": 105.9746564
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Đông Y Toàn Phúc Đường",
   "address": "Số 290/5 đường 30-4, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5820279,
   "Latitude": 105.9592456
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Lộc Tự",
   "address": "Ấp 3, thị trấn Phú Lộc, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.4241817,
   "Latitude": 105.7388798
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 09, Quốc lộ Nam Sông Hậu, Khóm Vĩnh Bình, Phường 2, thị xã Vĩnh Châu, Sóc Trăng",
   "Longtitude": 9.32138859999999,
   "Latitude": 105.983744
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe Nha Khoa Việt Đức",
   "address": "Số 112, Hùng Vương, Khóm 6, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6136385,
   "Latitude": 105.96808
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Phước Thiện Hưng Trị Tự",
   "address": "Ấp Đay Sô, xã Thạnh Quới, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.4573061,
   "Latitude": 105.7986909
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Nhi \"Huỳnh Mỹ Tâm\"",
   "address": "Số 41, Nguyễn Văn Thêm, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.600812,
   "Latitude": 105.973845
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "Số 46 Hoàng Diệu, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5593852,
   "Latitude": 105.9901982
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Khu 1, xã Thạnh Phú, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5070903,
   "Latitude": 105.8583784
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "143/24 Nguyễn Thị Minh Khai, Khóm 3, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6011002,
   "Latitude": 105.9755063
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp An Ninh 2, thị trấn Kế Sách, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": null,
   "Name": "Phòng Răng Bác Sỹ Bảo",
   "address": "Ấp Trà Quýt A, thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Dịch Vụ Chăm Sóc Sức Khỏe \"Hữu Nghị\" (Cô Chu)",
   "address": "Số 22 Nguyễn Thị Minh Khai, Khóm 6, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6011247,
   "Latitude": 105.9755265
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Ấp Trung Hòa, xã Tuân Tức, huyện Thạnh Trị, Sóc Trăng",
   "Longtitude": 9.488951,
   "Latitude": 105.7409852
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Khoa",
   "address": "Số 535, đường 30 tháng 4, Khóm 1, Phường 3, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.5907872,
   "Latitude": 105.98366
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Yhct Từ Thiện Tịnh Xã Ngọc Phước",
   "address": "Số 101/58 đường Hùng Vương, Khóm 1, Phường 6, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6140375,
   "Latitude": 105.9677336
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 568, đường Trần Phú, ấp Trà Quýt A thị trấn Châu Thành, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tiết Niệu - Nam Khoa",
   "address": "Số 768, ấp Ngãi Hội 1, thị trấn Đại Ngãi, huyện Long Phú, Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Sài Gòn Medic",
   "address": "Số 84, đường 934, ấp Châu Thành, thị trấn Mỹ Xuyên, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.5589648,
   "Latitude": 105.9926421
 },
 {
   "STT": null,
   "Name": "Nha Khoa Tuấn Dân",
   "address": "Khóm 1, Phường 1, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.5614498,
   "Latitude": 105.6063045
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp",
   "address": "Số 277, Xóm 2, ấp Châu Thành, xã An Ninh, huyện Châu Thành, Sóc Trăng",
   "Longtitude": 9.6106087,
   "Latitude": 105.9053689
 },
 {
   "STT": null,
   "Name": "Phòng Khám Nội Tổng Hợp Bác sỹ A",
   "address": "Ấp Hòa Đặng, xã Ngọc Đông, huyện Mỹ Xuyên, Sóc Trăng",
   "Longtitude": 9.46898719999999,
   "Latitude": 105.9406128
 },
 {
   "STT": null,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vạn Hòa Đường",
   "address": "Ấp Long Hòa, xã Tân Long, thị xã Ngã Năm, Sóc Trăng",
   "Longtitude": 9.52503089999999,
   "Latitude": 105.6588485
 },
 {
   "STT": null,
   "Name": "Phòng Răng Sài Gòn",
   "address": "Ấp An Ninh 2, thị trấn An Lạc Thôn, huyện Kế Sách, Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
];