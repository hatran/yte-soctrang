var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm Truyền thông giáo dục sức khỏe Sóc Trăng",
   "area": "Tỉnh",
   "address": "71 3 Tháng 2, Phường 4, Sóc Trăng, Việt Nam",
   "Longtitude": 9.6048015,
   "Latitude": 105.9743946
 },
 {
   "STT": 2,
   "Name": "Trung tâm Phòng, chống bệnh xã hội Sóc Trăng",
   "area": "Tỉnh",
   "address": "412 Lê Hồng Phong, thành phố Sóc Trăng, Sóc Trăng, Việt Nam",
   "Longtitude": 9.5809003,
   "Latitude": 105.9745812
 },
 {
   "STT": 3,
   "Name": "Trung tâm Kiểm nghiệm Thuốc, Mỹ phẩm và Thực phẩm Sóc Trăng",
   "area": "Tỉnh",
   "address": "52 Hùng Vương, Phường 6, Sóc Trăng, Việt Nam",
   "Longtitude": 9.6165539,
   "Latitude": 105.9636658
 },
 {
   "STT": 4,
   "Name": "Trung tâm Y tế dự phòng Sóc Trăng",
   "area": "Tỉnh",
   "address": "224 Lý Thường Kiệt, Phường 4, Sóc Trăng, Việt Nam",
   "Longtitude": 9.5924741,
   "Latitude": 105.9685133
 },
 {
   "STT": 5,
   "Name": "Trung tâm Phòng, chống HIV/AIDS Sóc Trăng",
   "area": "Tỉnh",
   "address": "374 Lê Duẩn, Khóm 5, thành phố Sóc Trăng, Sóc Trăng, Việt Nam",
   "Longtitude": 9.5958203,
   "Latitude": 105.9846316
 },
 {
   "STT": 6,
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản Sóc Trăng",
   "area": "Tỉnh",
   "address": "Phường 6, Thành Phố Sóc Trăng, Sóc Trăng, Việt Nam",
   "Longtitude": 9.607304,
   "Latitude": 105.9737739
 },
 {
   "STT": 7,
   "Name": "Trung tâm Pháp y Sóc Trăng",
   "area": "Tỉnh",
   "address": "39 Nguyễn Văn Thêm, Phường 3, Sóc Trăng, Việt Nam",
   "Longtitude": 9.6095825,
   "Latitude": 105.9705091
 },
 {
   "STT": 8,
   "Name": "Trung tâm Giám định y khoa Sóc Trăng",
   "area": "Tỉnh",
   "address": "4 Pasteur, Phường 5, Sóc Trăng, Việt Nam",
   "Longtitude": 9.6095826,
   "Latitude": 105.9770752
 },
 {
   "STT": 9,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình Sóc Trăng",
   "area": "Tỉnh",
   "address": "Số 8, Châu Văn Tiếp, P2, thành phố Sóc Trăng",
   "Longtitude": 9.5988087,
   "Latitude": 105.9692207
 },
 {
   "STT": 10,
   "Name": "Chi cục An toàn vệ sinh thực phẩm Sóc Trăng",
   "area": "Tỉnh",
   "address": "232 Lý Thường Kiệt, Phường 4, thành phố Sóc Trăng, Sóc Trăng",
   "Longtitude": 9.6054607,
   "Latitude": 105.9765143
 }
];