var dataptramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1 ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.330204,
   "Latitude": 105.9788011
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Xã Hòa Đông",
   "address": "Xã Hòa Đông ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4250794,
   "Latitude": 106.0757749
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Phường Khánh Hòa",
   "address": "Phường Khánh Hòa ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4068611,
   "Latitude": 106.0052437
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Xã Vĩnh Hiệp",
   "address": "Xã Vĩnh Hiệp ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3875385,
   "Latitude": 105.9582372
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Xã Vĩnh Hải",
   "address": "Xã Vĩnh Hải ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3788328,
   "Latitude": 106.1463317
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Xã Lạc Hòa",
   "address": "Xã Lạc Hòa ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3821101,
   "Latitude": 106.0757749
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2 ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3413311,
   "Latitude": 106.0287512
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Phường Vĩnh Phước",
   "address": "Phường Vĩnh Phước ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.324166,
   "Latitude": 105.9347384
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Xã Vĩnh Tân",
   "address": "Xã Vĩnh Tân ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3263079,
   "Latitude": 105.8877494
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Xã Lai Hòa",
   "address": "Xã Lai Hòa ,Thị xã Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3069609,
   "Latitude": 105.8407722
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1 ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5610689,
   "Latitude": 105.6064976
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2 ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.59538349999999,
   "Latitude": 105.652983
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Xã Vĩnh Quới",
   "address": "Xã Vĩnh Quới ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5724495,
   "Latitude": 105.5650239
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Xã Tân Long",
   "address": "Xã Tân Long ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.52503089999999,
   "Latitude": 105.6588485
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Xã Long Bình",
   "address": "Xã Long Bình ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.53688249999999,
   "Latitude": 105.6353878
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3 ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.51234019999999,
   "Latitude": 105.5826123
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Xã Mỹ Bình",
   "address": "Xã Mỹ Bình ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4845999,
   "Latitude": 105.6002024
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Xã Mỹ Quới",
   "address": "Xã Mỹ Quới ,Thị xã Ngã Năm ,Tỉnh Sóc Trăng",
   "Longtitude": 9.453873,
   "Latitude": 105.5650239
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Phường 5",
   "address": "Phường 5 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.62299569999999,
   "Latitude": 105.9837176
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Phường 7",
   "address": "Phường 7 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.62628,
   "Latitude": 105.9583852
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Phường 8",
   "address": "Phường 8 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6165731,
   "Latitude": 106.0107648
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Phường 6",
   "address": "Phường 6 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6104596,
   "Latitude": 105.9710768
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5950511,
   "Latitude": 105.9581887
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60537849999999,
   "Latitude": 105.9736599
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Phường 4",
   "address": "Phường 4 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6020906,
   "Latitude": 105.9906279
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5857493,
   "Latitude": 105.9758633
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Phường 9",
   "address": "Phường 9 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5861126,
   "Latitude": 105.9940424
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10 ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5789119,
   "Latitude": 105.9494248
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Đại Ân  2",
   "address": "Xã Đại Ân  2 ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.55589279999999,
   "Latitude": 106.1522126
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Thị trấn Trần Đề",
   "address": "Thị trấn Trần Đề ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.52139399999999,
   "Latitude": 106.199266
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Liêu Tú",
   "address": "Xã Liêu Tú ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4822703,
   "Latitude": 106.1169299
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Xã Lịch Hội Thượng",
   "address": "Xã Lịch Hội Thượng ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4432738,
   "Latitude": 106.1463317
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Thị trấn Lịch Hội Thượng",
   "address": "Thị trấn Lịch Hội Thượng ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4862636,
   "Latitude": 106.1463317
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Xã Trung Bình",
   "address": "Xã Trung Bình ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.484046,
   "Latitude": 106.1933837
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Tài Văn",
   "address": "Xã Tài Văn ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5563419,
   "Latitude": 106.0287512
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Viên An",
   "address": "Xã Viên An ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.51729469999999,
   "Latitude": 106.0581397
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Thạnh Thới An",
   "address": "Xã Thạnh Thới An ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4935651,
   "Latitude": 106.0060112
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Thạnh Thới Thuận",
   "address": "Xã Thạnh Thới Thuận ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4411162,
   "Latitude": 105.992634
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Viên Bình",
   "address": "Xã Viên Bình ,Huyện Trần Đề ,Tỉnh Sóc Trăng",
   "Longtitude": 9.48392479999999,
   "Latitude": 106.0816536
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Thị trấn Phú Lộc",
   "address": "Thị trấn Phú Lộc ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Thị trấn Hưng Lợi",
   "address": "Thị trấn Hưng Lợi ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4157373,
   "Latitude": 105.6940454
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Lâm Tân",
   "address": "Xã Lâm Tân ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5252993,
   "Latitude": 105.7703287
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Thạnh Tân",
   "address": "Xã Thạnh Tân ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5121067,
   "Latitude": 105.7057792
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Lâm Kiết",
   "address": "Xã Lâm Kiết ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5174757,
   "Latitude": 105.8231588
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Tuân Tức",
   "address": "Xã Tuân Tức ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.488951,
   "Latitude": 105.7409852
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Vĩnh Thành",
   "address": "Xã Vĩnh Thành ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4660449,
   "Latitude": 105.652983
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Thạnh Trị",
   "address": "Xã Thạnh Trị ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4639086,
   "Latitude": 105.6999122
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Vĩnh Lợi",
   "address": "Xã Vĩnh Lợi ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4409843,
   "Latitude": 105.6119301
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Châu Hưng",
   "address": "Xã Châu Hưng ,Huyện Thạnh Trị ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4229786,
   "Latitude": 105.652983
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Thị trấn Mỹ Xuyên",
   "address": "Thị trấn Mỹ Xuyên ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5528908,
   "Latitude": 105.9876149
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Đại Tâm",
   "address": "Xã Đại Tâm ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.54542,
   "Latitude": 105.9171161
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Tham Đôn",
   "address": "Xã Tham Đôn ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.517683,
   "Latitude": 105.9347384
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Thạnh Phú",
   "address": "Xã Thạnh Phú ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5010332,
   "Latitude": 105.8494878
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Ngọc Đông",
   "address": "Xã Ngọc Đông ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.46898719999999,
   "Latitude": 105.9406128
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Thạnh Quới",
   "address": "Xã Thạnh Quới ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4573061,
   "Latitude": 105.7986909
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Hòa Tú 1",
   "address": "Xã Hòa Tú 1 ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4496457,
   "Latitude": 105.8936224
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Gia Hòa 1",
   "address": "Xã Gia Hòa 1 ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4297548,
   "Latitude": 105.8583873
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã Ngọc Tố",
   "address": "Xã Ngọc Tố ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.42597,
   "Latitude": 105.9406128
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Gia Hòa 2",
   "address": "Xã Gia Hòa 2 ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.39964099999999,
   "Latitude": 105.8114175
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Hòa Tú II",
   "address": "Xã Hòa Tú II ,Huyện Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4496457,
   "Latitude": 105.8936224
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Thị trấn Huỳnh Hữu Nghĩa",
   "address": "Thị trấn Huỳnh Hữu Nghĩa ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.63711949999999,
   "Latitude": 105.7996769
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Xã Long Hưng",
   "address": "Xã Long Hưng ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6967403,
   "Latitude": 105.7938069
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Hưng Phú",
   "address": "Xã Hưng Phú ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6568607,
   "Latitude": 105.7233814
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Xã Mỹ Hương",
   "address": "Xã Mỹ Hương ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6349229,
   "Latitude": 105.8466437
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Mỹ Tú",
   "address": "Xã Mỹ Tú ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60586,
   "Latitude": 105.776198
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Mỹ Phước",
   "address": "Xã Mỹ Phước ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6034617,
   "Latitude": 105.7116464
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Thuận Hưng",
   "address": "Xã Thuận Hưng ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5668892,
   "Latitude": 105.8497189
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Mỹ Thuận",
   "address": "Xã Mỹ Thuận ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5662234,
   "Latitude": 105.8172881
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Phú Mỹ",
   "address": "Xã Phú Mỹ ,Huyện Mỹ Tú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.57509589999999,
   "Latitude": 105.9100914
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Thị trấn Long Phú",
   "address": "Thị trấn Long Phú ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6221576,
   "Latitude": 106.1169299
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Song Phụng",
   "address": "Xã Song Phụng ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7548959,
   "Latitude": 106.0463837
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Thị trấn Đại Ngãi",
   "address": "Thị trấn Đại Ngãi ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã Hậu Thạnh",
   "address": "Xã Hậu Thạnh ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7230926,
   "Latitude": 106.0346285
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã Long Đức",
   "address": "Xã Long Đức ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6884921,
   "Latitude": 106.0816536
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Trường Khánh",
   "address": "Xã Trường Khánh ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6810748,
   "Latitude": 106.0111203
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Phú Hữu",
   "address": "Xã Phú Hữu ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6896158,
   "Latitude": 106.0581397
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Tân Hưng",
   "address": "Xã Tân Hưng ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6238356,
   "Latitude": 106.0816536
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Châu Khánh",
   "address": "Xã Châu Khánh ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6470598,
   "Latitude": 106.0463837
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Xã Tân Thạnh",
   "address": "Xã Tân Thạnh ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5983024,
   "Latitude": 106.0522616
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Long Phú",
   "address": "Xã Long Phú ,Huyện Long Phú ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5734287,
   "Latitude": 106.1228099
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Thị trấn Kế Sách",
   "address": "Thị trấn Kế Sách ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7690706,
   "Latitude": 105.9758633
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Thị trấn An Lạc Thôn",
   "address": "Thị trấn An Lạc Thôn ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.8901831,
   "Latitude": 105.9288641
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Xuân Hòa",
   "address": "Xã Xuân Hòa ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.9210485,
   "Latitude": 105.8885484
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Phong Nẫm",
   "address": "Xã Phong Nẫm ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.8970152,
   "Latitude": 105.9552997
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã An Lạc Tây",
   "address": "Xã An Lạc Tây ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.844109,
   "Latitude": 105.9876149
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Trinh Phú",
   "address": "Xã Trinh Phú ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.8469393,
   "Latitude": 105.9288641
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Ba Trinh",
   "address": "Xã Ba Trinh ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.821893,
   "Latitude": 105.8877494
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Thới An Hội",
   "address": "Xã Thới An Hội ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7957824,
   "Latitude": 105.981739
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã Nhơn Mỹ",
   "address": "Xã Nhơn Mỹ ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.79352179999999,
   "Latitude": 106.0287512
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Kế Thành",
   "address": "Xã Kế Thành ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7707563,
   "Latitude": 105.9406128
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Kế An",
   "address": "Xã Kế An ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7718779,
   "Latitude": 105.9171161
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Xã Đại Hải",
   "address": "Xã Đại Hải ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.77867779999999,
   "Latitude": 105.8877494
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã An Mỹ",
   "address": "Xã An Mỹ ,Huyện Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7298916,
   "Latitude": 106.0052437
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Thị trấn Cù Lao Dung",
   "address": "Thị trấn Cù Lao Dung ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6717816,
   "Latitude": 106.1492721
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã An Thạnh 1",
   "address": "Xã An Thạnh 1 ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7304947,
   "Latitude": 106.1051705
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã An Thạnh Tây",
   "address": "Xã An Thạnh Tây ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6862394,
   "Latitude": 106.1286901
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã An Thạnh Đông",
   "address": "Xã An Thạnh Đông ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6505306,
   "Latitude": 106.199266
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Đại Ân 1",
   "address": "Xã Đại Ân 1 ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60858599999999,
   "Latitude": 106.1757379
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã An Thạnh 2",
   "address": "Xã An Thạnh 2 ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.607462,
   "Latitude": 106.199266
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã An Thạnh 3",
   "address": "Xã An Thạnh 3 ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5667077,
   "Latitude": 106.2639825
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã An Thạnh Nam",
   "address": "Xã An Thạnh Nam ,Huyện Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5248153,
   "Latitude": 106.2404468
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Thị trấn Châu Thành",
   "address": "Thị trấn Châu Thành ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.70766759999999,
   "Latitude": 105.9053689
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Hồ Đắc Kiện",
   "address": "Xã Hồ Đắc Kiện ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7150088,
   "Latitude": 105.8642593
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Phú Tâm",
   "address": "Xã Phú Tâm ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.7162278,
   "Latitude": 105.9523622
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Thuận Hòa",
   "address": "Xã Thuận Hòa ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Phú Tân",
   "address": "Xã Phú Tân ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.67307999999999,
   "Latitude": 105.9523622
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Thiện Mỹ",
   "address": "Xã Thiện Mỹ ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.67751659999999,
   "Latitude": 105.8583873
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã An Hiệp",
   "address": "Xã An Hiệp ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.646956,
   "Latitude": 105.9347384
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã An Ninh",
   "address": "Xã An Ninh ,Huyện Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6106087,
   "Latitude": 105.9053689
 }
];