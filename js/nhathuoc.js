var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Nhà thuốc Nhan Hoa",
   "address": "72 Phan Chu Trinh ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6040757,
   "Latitude": 105.9741323
 },
 {
   "STT": 2,
   "Name": "Nhà thuốc Nguyen Hue",
   "address": "372a Nguyen Hue ,Tỉnh Sóc Trăng",
   "Longtitude": 9.601861,
   "Latitude": 105.9764226
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Thanh Truc",
   "address": "7 Nguyen Hung Phuoc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.603818,
   "Latitude": 105.9751445
 },
 {
   "STT": 4,
   "Name": "Nhà thuốc Tram Anh",
   "address": "63 A Long Phu,  Phường 8 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6097661,
   "Latitude": 105.9808605
 },
 {
   "STT": 5,
   "Name": "Cty Tnhh Dp Nhut Tan",
   "address": "06 Tran Minh Phu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.605608,
   "Latitude": 105.9749108
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Minh Tri",
   "address": "45a Phan Chu Trinh ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60445189999999,
   "Latitude": 105.9731277
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Quang Thao",
   "address": "123 Tran Hung Dao ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5963952,
   "Latitude": 105.9709528
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Hoa Binh",
   "address": "41 Dinh Tien Hoang ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6052199,
   "Latitude": 105.9738516
 },
 {
   "STT": 9,
   "Name": "Nhà thuốc Bao Toan",
   "address": "50 Phan Chu Trinh ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6036402,
   "Latitude": 105.9747151
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Hong Oanh",
   "address": "6 Nguyen Hung Phuoc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6037115,
   "Latitude": 105.9752917
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc My Linh",
   "address": "12 Tran Minh Phu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6057645,
   "Latitude": 105.974848
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc Anh Tuan",
   "address": "8 Cach Mang Thang 8 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6035607,
   "Latitude": 105.97364
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc Thanh Huong",
   "address": "228b Ly Thuong Kiet, K1,  Phường 4 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60600339999999,
   "Latitude": 105.9744993
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc Minh Duc",
   "address": "18 Xo Viet Nghe Tinh-phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6035746,
   "Latitude": 105.9723169
 },
 {
   "STT": 15,
   "Name": "Cong Ty Co Phan Duoc S.Pharm",
   "address": "45-47 Hai Ba Trung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6032726,
   "Latitude": 105.9741419
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc Thanh Huong",
   "address": "228b Ly Thuong Kiet, K1,  Phường 4 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60600339999999,
   "Latitude": 105.9744993
 },
 {
   "STT": 17,
   "Name": "Thị trấn Phong Chong Benh Xa Hoi",
   "address": "412 Le Hong Phong Soc Trang ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5828169,
   "Latitude": 105.9763777
 },
 {
   "STT": 18,
   "Name": "Ban Bvcssk Can Bo Soc Trang",
   "address": "So 01 Tran Quang Dieu P2 Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5995186,
   "Latitude": 105.9717673
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Le Khanh",
   "address": "47 Hoang Dieu,Thị trấn My Xuyen ,Tỉnh Sóc Trăng",
   "Longtitude": 9.559253,
   "Latitude": 105.9901714
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Thanh Nhan",
   "address": "28 Hoang Dieu-My Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.56007009999999,
   "Latitude": 105.9901251
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Anh Tuấn",
   "address": "Đường Cách Mạng Tháng 8, Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6037993,
   "Latitude": 105.9729394
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Vạn Xuân Viên",
   "address": "11, Phan Chu Trinh, Phường 1, Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6032189,
   "Latitude": 105.975542
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Minh Trí",
   "address": "45 Phan Chu Trinh, Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60445189999999,
   "Latitude": 105.9731277
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Vinh Lợi",
   "address": "07 Ngô Quyền, Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6034569,
   "Latitude": 105.9736009
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Vạn Hòa Đường",
   "address": "202 Lý Thường Kiệt, Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6054844,
   "Latitude": 105.9775569
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc Thanh Liêm",
   "address": "34 Khóm 2, Phương 1, Thị xã. Vĩnh Châu ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3241857,
   "Latitude": 105.9801782
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Hòa Trung Đường",
   "address": "32a,Hàm Nghi ,Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6043599,
   "Latitude": 105.9753554
 },
 {
   "STT": 29,
   "Name": "Cty Tnhh Mtv Nguyễn Huệ Pharm",
   "address": "06 Nguyễn Du- Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6024206,
   "Latitude": 105.9764141
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Vạn Hòa Đường",
   "address": "202 Lý Thuong Kiet; Phường 4 ;Thành phố Sóc Trăng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6054844,
   "Latitude": 105.9775569
 },
 {
   "STT": 31,
   "Name": "Nhà Thuốc Anh Tuấn Key",
   "address": "8 Cách Mạng Tháng 8, Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6035607,
   "Latitude": 105.97364
 },
 {
   "STT": 32,
   "Name": "Nhà Thuốc Hòa Trung Đường Key",
   "address": "32 Hàm Nghi, Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6043599,
   "Latitude": 105.9753554
 },
 {
   "STT": 33,
   "Name": "Cơ Sở Bán Thuốc Đông Y Thành Lợi Key",
   "address": "44-Nguyễn Văn Cừ- Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.60499499999999,
   "Latitude": 105.973273
 },
 {
   "STT": 34,
   "Name": "Nhà Thuốc Lợi Hòa Đường Key",
   "address": "117 Đường 3/2- Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.56315229999999,
   "Latitude": 105.5951994
 },
 {
   "STT": 35,
   "Name": "Nhà Thuốc Vạn Hòa Đường Key",
   "address": "202 Lý Thường Kiệt, Phường 4 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6054844,
   "Latitude": 105.9775569
 },
 {
   "STT": 36,
   "Name": "Nhà Thuốc Đông Y Châm Cứu Key",
   "address": "29 Nguyễn Văn Trỗi- Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6049053,
   "Latitude": 105.9757752
 },
 {
   "STT": 37,
   "Name": "Nhà Thuốc Vạn Xuân Viên Key",
   "address": "11 Phan Chu Trinh - Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6032189,
   "Latitude": 105.975542
 },
 {
   "STT": 38,
   "Name": "Nhà Thuốc Minh Trí Key",
   "address": "25 -Phan Chu Trinh - Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.3241857,
   "Latitude": 105.9801782
 },
 {
   "STT": 39,
   "Name": "Nhà Thuốc Thanh Trúc",
   "address": "07 Nguyễn Hùng Phước, Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.603818,
   "Latitude": 105.9751445
 },
 {
   "STT": 40,
   "Name": "Nhà Thuốc Vinh Hòa Đường",
   "address": "Ấp 1 -Thị trấn Phú Lộc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.55589279999999,
   "Latitude": 106.1522126
 },
 {
   "STT": 41,
   "Name": "Nhà Thuốc Hòa Bình Key",
   "address": "41- Đinh Tiên Hoàng- Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6052199,
   "Latitude": 105.9738516
 },
 {
   "STT": 42,
   "Name": "Cty Tnhh Dp Nhựt Tân Key",
   "address": "Số 6 Trần Minh Phú - Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.605608,
   "Latitude": 105.9749108
 },
 {
   "STT": 43,
   "Name": "Nhà Thuốc Phong Hòa Đường Key",
   "address": "411 Chợ Trà Quýt, Châu Thành ,Tỉnh Sóc Trăng",
   "Longtitude": 9.6753018,
   "Latitude": 105.9053689
 },
 {
   "STT": 44,
   "Name": "Nhà Thuốc Vạn Tế Đường Key",
   "address": "30 Đường 30/4-Thị trấn Kế Sách ,Tỉnh Sóc Trăng",
   "Longtitude": 9.77022019999999,
   "Latitude": 105.9850121
 },
 {
   "STT": 45,
   "Name": "Nhà Thuốc Như Ý Key",
   "address": "Đòan Thế Trung- Ấp Chợ- Cù Lao Dung ,Tỉnh Sóc Trăng",
   "Longtitude": 9.670667,
   "Latitude": 106.1565757
 },
 {
   "STT": 46,
   "Name": "Nhà Thuốc Nhơn Hòa Đường",
   "address": "Ấp 1, Thị trấn Đại Ngãi ,Tỉnh Sóc Trăng",
   "Longtitude": 9.73275359999999,
   "Latitude": 106.0581397
 },
 {
   "STT": 47,
   "Name": "Nhà Thuốc Bồi An Đường Key",
   "address": "2a- Triệu Nương - Thị trấn Mỹ Xuyên ,Tỉnh Sóc Trăng",
   "Longtitude": 9.55960039999999,
   "Latitude": 105.9899274
 },
 {
   "STT": 48,
   "Name": "Nhà Thuốc Xuân Hòa Đường Key",
   "address": "308 Đường 3/2 - Khóm 1 - Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5648193,
   "Latitude": 105.5973455
 },
 {
   "STT": 49,
   "Name": "Nhà Thuốc Vinh Nghĩa Đường",
   "address": "347 Mai Thanh Thế-K1-Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5628402,
   "Latitude": 105.5963065
 },
 {
   "STT": 50,
   "Name": "Quầy Thuốc Cẩm Tú",
   "address": "373 Mai Thanh Thế-K1-Phường 1 ,Tỉnh Sóc Trăng",
   "Longtitude": 9.5628402,
   "Latitude": 105.5963065
 },
 {
   "STT": 51,
   "Name": "Quầy Thuốc Quốc Bảo",
   "address": "Ấp 3, Thị trấn Phú Lộc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4241817,
   "Latitude": 105.7388798
 },
 {
   "STT": 52,
   "Name": "Nhà Thuốc Thanh Hiếu",
   "address": "255 Quốc Lộ 1, Thị trấn Phú Lộc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.429133,
   "Latitude": 105.7440197
 },
 {
   "STT": 53,
   "Name": "Nhà Thuốc Thiên Hòa Đường Key",
   "address": "170 Lý Thường Kiệt-Thị trấn Phú Lộc ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4243648,
   "Latitude": 105.7409852
 },
 {
   "STT": 54,
   "Name": "Nhà Thuốc Đức Thạnh Đường Key",
   "address": "273/3 Ấp Châu Thành - Lịch Hội Thượng ,Tỉnh Sóc Trăng",
   "Longtitude": 9.4647657,
   "Latitude": 106.1463317
 }
];